/**
 * 
 */
package com.ebanx.test.dtos;

/**
 * @author OluwasegunAjayi on 22nd September 2020
 *
 */
public class TransactionDTO {
	
	//private fields
	private String type;
	private String destination;
	private long amount;
	private String origin;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}

}
