/**
 * 
 */
package com.ebanx.test.dtos;

import com.ebanx.test.entities.Accounts;

/**
 * @author OluwasegunAjayi on 23rd September 2020
 *
 */
public class DepositDTO {
	
	//private fields
	private Accounts destination;

	public Accounts getDestination() {
		return destination;
	}

	public void setDestination(Accounts destination) {
		this.destination = destination;
	}

}
