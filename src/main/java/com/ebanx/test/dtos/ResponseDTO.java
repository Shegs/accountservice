/**
 * 
 */
package com.ebanx.test.dtos;

import com.ebanx.test.entities.Accounts;

/**
 * @author OluwasegunAjayi on 22nd September 2020
 *
 */
public class ResponseDTO {
	
	//private fields
	private Accounts origin;
	private Accounts destination;
	
	public Accounts getOrigin() {
		return origin;
	}
	public void setOrigin(Accounts origin) {
		this.origin = origin;
	}
	public Accounts getDestination() {
		return destination;
	}
	public void setDestination(Accounts destination) {
		this.destination = destination;
	}

}
