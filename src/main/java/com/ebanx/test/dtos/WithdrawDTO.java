/**
 * 
 */
package com.ebanx.test.dtos;

import com.ebanx.test.entities.Accounts;

/**
 * @author OluwasegunAjayi on 23rd September 2020
 *
 */
public class WithdrawDTO {
	
	//private fields
	private Accounts origin;

	public Accounts getOrigin() {
		return origin;
	}

	public void setOrigin(Accounts origin) {
		this.origin = origin;
	}

}
