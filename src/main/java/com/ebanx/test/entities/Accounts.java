/**
 * 
 */
package com.ebanx.test.entities;

/**
 * @author OluwasegunAjayi on 22nd September 2020
 *
 */
public class Accounts {
	
	//private fields
	private String id;
	private long balance;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public long getBalance() {
		return balance;
	}
	public void setBalance(long balance) {
		this.balance = balance;
	}

}
