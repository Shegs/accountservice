/**
 * 
 */
package com.ebanx.test.restcontrollers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ebanx.test.TestServiceApplication;
import com.ebanx.test.dtos.DepositDTO;
import com.ebanx.test.dtos.ResponseDTO;
import com.ebanx.test.dtos.TransactionDTO;
import com.ebanx.test.dtos.WithdrawDTO;
import com.ebanx.test.services.AccountsService;

/**
 * @author OluwasegunAjayi on 22nd September 2020
 *
 */
@CrossOrigin(origins = "*")
@RestController("accountsRestController")
public class AccountsRestController {
	
	//private fields
	private static final Logger logger = LoggerFactory.getLogger(TestServiceApplication.class);
	
	@Autowired
	private AccountsService accountsService;
	
	@RequestMapping(value = "/event", method = RequestMethod.POST)
	public ResponseEntity<?> event(@RequestBody TransactionDTO transactionDTO){
		logger.info("API Call To Carry Out Transaction");
		
		ResponseDTO response = accountsService.event(transactionDTO);
		if(response == null) {
			return new ResponseEntity<>(0, HttpStatus.NOT_FOUND);
		}else {
			if(response.getOrigin() == null) {
				DepositDTO depo = new DepositDTO();
				depo.setDestination(response.getDestination());
				return new ResponseEntity<>(depo, HttpStatus.CREATED);
			}else if(response.getDestination() == null) {
				WithdrawDTO with = new WithdrawDTO();
				with.setOrigin(response.getOrigin());
				return new ResponseEntity<>(with, HttpStatus.CREATED);
			}
			return new ResponseEntity<>(response, HttpStatus.CREATED);
		}
	}
	
	@RequestMapping(value = "/reset", method = RequestMethod.POST)
	public ResponseEntity<?> reset(){
		logger.info("API Call To Reset");
		
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/balance", method = RequestMethod.GET)
	public ResponseEntity<?> balance(@RequestParam("account_id") String id){
		logger.info("API Call To Get Account Balance");
		
		long bal = accountsService.balance(id);
		if(bal == -1) {
			return new ResponseEntity<>(0, HttpStatus.NOT_FOUND);
		}else {
			return new ResponseEntity<>(bal, HttpStatus.OK);
		}
	}

}
