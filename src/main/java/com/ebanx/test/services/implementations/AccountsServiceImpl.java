/**
 * 
 */
package com.ebanx.test.services.implementations;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ebanx.test.TestServiceApplication;
import com.ebanx.test.dtos.ResponseDTO;
import com.ebanx.test.dtos.TransactionDTO;
import com.ebanx.test.entities.Accounts;
import com.ebanx.test.services.AccountsService;

/**
 * @author OluwasegunAjayi on 22nd September 2020
 *
 */
@Service("accountsService")
public class AccountsServiceImpl implements AccountsService {
	
	//private fields
	private static final Logger logger = LoggerFactory.getLogger(TestServiceApplication.class);
	private HashMap<String, Accounts> accounts = new HashMap<>();

	@Override
	public ResponseDTO event(TransactionDTO transactionDTO) {
		logger.info("Carrying Out A New Transaction");
		
		ResponseDTO response = new ResponseDTO();
		//Validation
		/**if(transactionDTO.getType() == null || !transactionDTO.getType().equals("deposit") || !transactionDTO.getType().equals("withdraw") || !transactionDTO.getType().equals("transfer")) {
			return null;
		}**/
		
		try {
			if(transactionDTO.getType().equals("deposit")) {
				if(transactionDTO.getDestination() == null) {
					return null;
				}
				Accounts acct = accounts.get(transactionDTO.getDestination());
				Accounts newAcct = new Accounts();
				if(acct == null) {
					//Creating new account for deposits to accounts that do not exist before
					newAcct.setBalance(transactionDTO.getAmount());
					newAcct.setId(transactionDTO.getDestination());
					accounts.put(transactionDTO.getDestination(), newAcct);
				}else {
					//Updating An Existing Account
					newAcct = this.add(transactionDTO, acct);
				}
				
				response.setDestination(newAcct);
			}else if(transactionDTO.getType().equals("withdraw")) {
				if(transactionDTO.getOrigin() == null) {
					return null;
				}
				Accounts acct = accounts.get(transactionDTO.getOrigin());
				if(acct == null) {
					//Withdrawing from an account that do not exist before
					return null;
				}else {
					//Updating An Existing Account
					response.setOrigin(this.deduct(transactionDTO, acct));
				}
			}else if(transactionDTO.getType().equals("transfer")) {
				if(transactionDTO.getOrigin() == null || transactionDTO.getDestination() == null) {
					return null;
				}
				//Checking Withdrawal first from origin account
				Accounts originAcct = accounts.get(transactionDTO.getOrigin());
				if(originAcct == null) {
					//Withdrawing from an account that do not exist before
					return null;
				}else {
					//Updating An Existing Account
					response.setOrigin(this.deduct(transactionDTO, originAcct));
				}
				//Top-up the destination account afterwards
				Accounts destAcct = accounts.get(transactionDTO.getDestination());
				Accounts newAcct = new Accounts();
				if(destAcct == null) {
					//Creating new account for deposits to accounts that do not exist before
					newAcct.setBalance(transactionDTO.getAmount());
					newAcct.setId(transactionDTO.getDestination());
					accounts.put(transactionDTO.getDestination(), newAcct);
				}else {
					//Updating An Existing Account
					newAcct = this.add(transactionDTO, destAcct);
				}
				
				response.setDestination(newAcct);
			}
			
			return response;
		}catch(Exception e) {
			logger.error("Error While Carrying Out A New Transaction "+e);
			return null;
		}
	}

	@Override
	public long balance(String id) {
		logger.info("Getting Account Balance");
		
		Accounts acct = accounts.get(id);
		if(acct == null) return -1;
		return acct.getBalance();
	}
	
	//Method to add to account balance
	private Accounts add(TransactionDTO transDTO, Accounts acct) {
		logger.info("Adding To Account Balance");
		
		Accounts newAcct = new Accounts();
		newAcct.setBalance(acct.getBalance() + transDTO.getAmount());
		newAcct.setId(acct.getId());
		accounts.replace(transDTO.getDestination(), newAcct);
		
		return newAcct;
	}
	
	//Method to deduct from account balance
	private Accounts deduct(TransactionDTO transDTO, Accounts acct) {
		logger.info("Deducting Account Balance");
			
		Accounts newAcct = new Accounts();
		newAcct.setBalance(acct.getBalance() - transDTO.getAmount());
		newAcct.setId(acct.getId());
		accounts.replace(transDTO.getOrigin(), newAcct);
			
		return newAcct;
	}

}
