/**
 * 
 */
package com.ebanx.test.services;

import com.ebanx.test.dtos.ResponseDTO;
import com.ebanx.test.dtos.TransactionDTO;

/**
 * @author OluwasegunAjayi on 22nd September 2020
 *
 */
public interface AccountsService {
	
	//Service Methods
	ResponseDTO event(TransactionDTO transactionDTO);
	long balance(String id);

}
